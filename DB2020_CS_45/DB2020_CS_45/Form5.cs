﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_45
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
            xyz();
      
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        public void xyz()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Student.Id from Student Except(Select GroupStudent.StudentId from GroupStudent)", con);
           
            SqlDataAdapter da = new SqlDataAdapter(cmd);
           
            DataSet ds = new DataSet();
            da.Fill(ds, "Student");
            comboBox2.ValueMember = "Id";
            comboBox2.DataSource = ds.Tables["Student"];


            SqlCommand cmd1 = new SqlCommand("Select [Group].Id FROM [Group] Except (Select GroupStudent.GroupId from GroupStudent)", con);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataSet ds1 = new DataSet();
            da1.Fill(ds1, "[Group]");
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = ds1.Tables["[Group]"];

        }
        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
           



        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();


            

            
            
            SqlCommand cmd = new SqlCommand("Insert into GroupStudent values (@GroupId,@StudentId,@Status,@AssignmentDate)", con);
            SqlCommand farrukh = new SqlCommand("Select Id from Student ", con);
            SqlCommand farr1 = new SqlCommand("Select (Id) from [Group] ", con);
            cmd.Parameters.AddWithValue("@GroupId", comboBox1.Text);
            //  SqlCommand farr = new SqlCommand("Select MAX(Id) from [Group] ", con);
            cmd.Parameters.AddWithValue("@StudentId", comboBox2.Text);
            

            cmd.Parameters.AddWithValue("@Status", textBox1.Text);
            cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Value);

           
      
     
          

   
            cmd.ExecuteNonQuery();
          
            MessageBox.Show("Successfully saved");


           
        
        }

        private void Form5_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();






            SqlCommand cmd = new SqlCommand("Insert into [Group] values (@Created_On)", con);
            cmd.Parameters.AddWithValue("@Created_On",DateTime.Now );



            SqlCommand cmd1 = new SqlCommand("Select Id FROM [Group]", con);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataSet ds1 = new DataSet();
            da1.Fill(ds1, "[Group]");
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = ds1.Tables["[Group]"];



            cmd.ExecuteNonQuery();

            MessageBox.Show("Successfully created");
        }
    }
}
