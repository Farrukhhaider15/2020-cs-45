﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_45
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
            xyz1();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void xyz1()
        {

            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Project.Id from  Project Except (SELECT GroupProject.ProjectId FROM  GroupProject)", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "Project");
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = ds.Tables["Project"];


            SqlCommand cmd1 = new SqlCommand(" select [Group].Id from  [Group] Except (SELECT GroupProject.GroupId FROM  GroupProject)", con);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataSet ds1 = new DataSet();
            da1.Fill(ds1, "[Group]");
            comboBox2.ValueMember = "Id";
            comboBox2.DataSource = ds1.Tables["[Group]"];

        }
        private void Form6_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

          
            SqlCommand cmd = new SqlCommand("Insert into GroupProject values (@ProjectId,@GroupId,@AssignmentDate)", con);
            SqlCommand farr = new SqlCommand("Select Id from Project ", con);
            SqlCommand farrukh = new SqlCommand("Select Id from [Group] ", con);
            cmd.Parameters.AddWithValue("@ProjectId", comboBox1.Text);
            cmd.Parameters.AddWithValue("@GroupId", comboBox2.Text);


           
            //SqlCommand cmd1 = new SqlCommand("select GroupProject.ProjectId from  GroupProject Except (SELECT GroupProject.ProjectId FROM  GroupProject)", con);
            //SqlDataAdapter da = new SqlDataAdapter(cmd1);
            //DataSet ds = new DataSet();
            //da.Fill(ds, "Project");
            //comboBox1.ValueMember = "Id";
            //comboBox1.DataSource = ds.Tables["Project"];


            //SqlCommand cmd11 = new SqlCommand("select GroupProject.GroupId from  GroupProject Except (SELECT GroupProject.GroupId FROM  GroupProject)", con);
            //SqlDataAdapter da1 = new SqlDataAdapter(cmd11);
            //DataSet ds1 = new DataSet();
            //da1.Fill(ds1, "[Group]");
            //comboBox2.ValueMember = "Id";
            //comboBox2.DataSource = ds1.Tables["[Group]"];


            cmd.Parameters.AddWithValue("@AssignmentDate", dateTimePicker1.Value);
            cmd.ExecuteNonQuery();
      

            MessageBox.Show("Successfully saved");

        }
    }
}
