﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;
using System.Data;

namespace DB2020_CS_45
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Person.Id,LastName,FirstName,RegistrationNo,DateOfBirth,Email,Contact,Gender from Student,Person where Student.Id=Person.Id AND Student.RegistrationNo=@RegistrationNo ", con);
            cmd.Parameters.AddWithValue("@RegistrationNo", textBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
           DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
           
          
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
      
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            if (dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString() != "" && dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString() != "" && dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString() != "" && dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].FormattedValue.ToString() != "" && dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].FormattedValue.ToString() != "" && dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].FormattedValue.ToString() != "" && dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[6].FormattedValue.ToString() != "" && dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[7].FormattedValue.ToString()!="")
            {
                if (Regex.IsMatch(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].FormattedValue.ToString(), @"(^[2]{1}[0]{1}[0-9]{2}-[A-Z]{2}-[0-9]{3}$)"))
                {
                    if (Regex.IsMatch(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[6].FormattedValue.ToString(), @"((\+92)?(0092)?(92)?(0)?)(3)([0-9]{9})"))
                    {
                        if (Regex.IsMatch(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].FormattedValue.ToString(), @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"))
                        {
                            updatedata();
                        }
                        else
                        {
                            MessageBox.Show("Please correct the format of the email");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter correct contact number");
                    }

                }
                else
                {
                    MessageBox.Show("Please correct the format of the registration number");
                }
            }
            else
            {
                MessageBox.Show("Please fill all the fields");
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            //SqlCommand cmd = new SqlCommand("DELETE Student WHERE RegistrationNo= @RegistrationNo", con);
            SqlCommand cmd5 = new SqlCommand("DELETE [Group] WHERE Id= @Id", con);
            SqlCommand cmd3 = new SqlCommand("DELETE GroupStudent WHERE StudentId= @Id", con);
            SqlCommand cmd4 = new SqlCommand("DELETE Student WHERE Id= @Id", con);
            SqlCommand cmd2 = new SqlCommand("DELETE Person WHERE Id= @Id", con);
           
            cmd5.Parameters.AddWithValue("@Id", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
            cmd4.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
            cmd2.Parameters.AddWithValue("@Id", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
            cmd3.Parameters.AddWithValue("@Id", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());

            cmd5.ExecuteNonQuery();

            cmd3.ExecuteNonQuery();
            cmd4.ExecuteNonQuery();
            cmd2.ExecuteNonQuery();
       

            MessageBox.Show("Data Deleted Successfully");
        }
        private void updatedata()
        {
          //  MessageBox.Show(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString());
           // MessageBox.Show(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Person SET FirstName=@FirstName,LastName=@LastName,Contact=@Contact,Email=@Email,DateOfBirth=@DateOfBirth,Gender=@Gender where ID=@ID ", con);
            cmd.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@LastName", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@Contact", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[6].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@Email", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[5].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@DateOfBirth", DateTime.Parse(dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[4].FormattedValue.ToString()));
            cmd.Parameters.AddWithValue("@Gender", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[7].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@FirstName", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString());


            SqlCommand cmd1 = new SqlCommand("UPDATE Student SET RegistrationNo=@RegistrationNo where ID=@ID", con);

            cmd1.Parameters.AddWithValue("@RegistrationNo", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[3].FormattedValue.ToString());
            cmd1.Parameters.AddWithValue("@ID", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());

            cmd1.ExecuteNonQuery();
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data Updated Successfully");
        }
    }
}
