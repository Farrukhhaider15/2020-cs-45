﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.IO;
using System.Data.SqlClient;




namespace DB2020_CS_45
{
    public partial class Form11 : Form
    {
        public Form11()
        {
            InitializeComponent();
            dataGridView1.Hide();
        }

        private void Form11_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select ProjectAdvisor.AdvisorId as AdvisorID, ProjectAdvisor.ProjectId as ProjectId ,Student.RegistrationNo as RegistrationNo from ProjectAdvisor cross join  Advisor cross join [Group] cross join Student join Project on Project.Id=ProjectAdvisor.ProjectId and Advisor.Id=ProjectAdvisor.AdvisorId join GroupProject on GroupProject.GroupId =[Group].Id and GroupProject.ProjectId=ProjectAdvisor.ProjectId  join GroupStudent ON GroupStudent.GroupId=[Group].Id and GroupStudent.StudentId=Student.Id", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;





            Document dc = new Document();
            PdfWriter.GetInstance(dc,new FileStream("D:/4th Semester/pdfs/testreport.pdf", FileMode.Create)); 
            string data = "University of Engineering and Technology,Lahore";
            var p1 = new Paragraph(data, FontFactory.GetFont("Arial", 19, iTextSharp.text.Font.BOLD));
            p1.Alignment = Element.ALIGN_CENTER;

            p1.SpacingAfter = 20f;

            dc.Open();

            dc.Add(p1);
            

            PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);



            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.Padding = 6;
            table.DefaultCell.PaddingTop = 10;
            table.WidthPercentage = 100;
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                table.AddCell(new Phrase(dataGridView1.Columns[j].HeaderText, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
            }
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                table.WidthPercentage = 100;
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        table.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), FontFactory.GetFont("Arial", 10, BaseColor.BLACK)));
                    }
                }
            }
            dc.Add(table);
            dc.Close();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Project.Id as ProjectID, Student.RegistrationNo AS RegistrationNo,GroupEvaluation.EvaluationId as EvaluationId,GroupEvaluation.ObtainedMarks as ObtainedMarks,Evaluation.TotalMarks as TotalMarks from GroupEvaluation join Evaluation on Evaluation.Id=GroupEvaluation.EvaluationId join [Group] on [Group].Id=GroupEvaluation.GroupId JOIN GroupStudent ON GroupStudent.GroupId=[Group].Id join Student on Student.Id=GroupStudent.GroupId  JOIN GroupProject on GroupProject.GroupId=[Group].Id join Project on  Project.Id=GroupProject.ProjectId", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;





            Document dc = new Document();
            PdfWriter.GetInstance(dc, new FileStream("D:/4th Semester/pdfs/report.pdf", FileMode.Create));
            string data = "University of Engineering and Technology,Lahore";
            var p1 = new Paragraph(data, FontFactory.GetFont("Arial", 19, iTextSharp.text.Font.BOLD));
            p1.Alignment = Element.ALIGN_CENTER;

            p1.SpacingAfter = 20f;

            dc.Open();

            dc.Add(p1);


            PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);



            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.Padding = 6;
            table.DefaultCell.PaddingTop = 10;
            table.WidthPercentage = 100;
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                table.AddCell(new Phrase(dataGridView1.Columns[j].HeaderText, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
            }
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                table.WidthPercentage = 100;
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        table.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), FontFactory.GetFont("Arial", 10, BaseColor.BLACK)));
                    }
                }
            }
            dc.Add(table);
            dc.Close();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Student.Id as StudentId,Evaluation.Name as Name,Evaluation.TotalMarks AS TotalMarks,[Group].Id as GroupId,ObtainedMarks from Student join GroupStudent ON GroupStudent.StudentId=Student.Id join [Group] on [Group].Id=GroupStudent.GroupId join GroupEvaluation on [Group].Id=GroupEvaluation.EvaluationId join Evaluation on Evaluation.Id=GroupEvaluation.EvaluationId where ObtainedMarks<50", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;





            Document dc = new Document();
            PdfWriter.GetInstance(dc, new FileStream("D:/4th Semester/pdfs/report3.pdf", FileMode.Create));
            string data = "University of Engineering and Technology,Lahore";
            var p1 = new Paragraph(data, FontFactory.GetFont("Arial", 19, iTextSharp.text.Font.BOLD));
            p1.Alignment = Element.ALIGN_CENTER;

            p1.SpacingAfter = 20f;

            dc.Open();

            dc.Add(p1);


            PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);



            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.Padding = 6;
            table.DefaultCell.PaddingTop = 10;
            table.WidthPercentage = 100;
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                table.AddCell(new Phrase(dataGridView1.Columns[j].HeaderText, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
            }
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                table.WidthPercentage = 100;
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        table.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), FontFactory.GetFont("Arial", 10, BaseColor.BLACK)));
                    }
                }
            }
            dc.Add(table);
            dc.Close();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select [Group].Id as GroupId,Project.Id as ProjectId,ProjectAdvisor.AdvisorRole as AdvisorRole from Advisor join ProjectAdvisor on ProjectAdvisor.AdvisorId=Advisor.Id join Project on Project.Id=ProjectAdvisor.ProjectId join GroupProject on GroupProject.ProjectId=Project.Id join [Group] on [Group].Id=GroupProject.GroupId where [Group].Id is not null and ProjectAdvisor.AdvisorRole is not null", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;





            Document dc = new Document();
            PdfWriter.GetInstance(dc, new FileStream("D:/4th Semester/pdfs/report4.pdf", FileMode.Create));
            string data = "University of Engineering and Technology,Lahore";
            var p1 = new Paragraph(data, FontFactory.GetFont("Arial", 19, iTextSharp.text.Font.BOLD));
            p1.Alignment = Element.ALIGN_CENTER;

            p1.SpacingAfter = 20f;

            dc.Open();

            dc.Add(p1);


            PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);



            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.Padding = 6;
            table.DefaultCell.PaddingTop = 10;
            table.WidthPercentage = 100;
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                table.AddCell(new Phrase(dataGridView1.Columns[j].HeaderText, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
            }
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                table.WidthPercentage = 100;
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        table.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), FontFactory.GetFont("Arial", 10, BaseColor.BLACK)));
                    }
                }
            }
            dc.Add(table);
            dc.Close();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Status,RegistrationNo,(FirstName+LastName) as FullName  from [Group]  join GroupStudent on [Group].Id=GroupStudent.GroupId  JOIN Student ON Student.Id=GroupStudent.StudentId  join Person on Person.Id=Student.Id group by Status,RegistrationNo, (FirstName+LastName) having status =3", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;





            Document dc = new Document();
            PdfWriter.GetInstance(dc, new FileStream("D:/4th Semester/pdfs/report5.pdf", FileMode.Create));
            string data = "University of Engineering and Technology,Lahore";
            var p1 = new Paragraph(data, FontFactory.GetFont("Arial", 19, iTextSharp.text.Font.BOLD));
            p1.Alignment = Element.ALIGN_CENTER;

            p1.SpacingAfter = 20f;

            dc.Open();

            dc.Add(p1);


            PdfPTable table = new PdfPTable(dataGridView1.Columns.Count);



            table.DefaultCell.HorizontalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.VerticalAlignment = Element.ALIGN_CENTER;
            table.DefaultCell.Padding = 6;
            table.DefaultCell.PaddingTop = 10;
            table.WidthPercentage = 100;
            for (int j = 0; j < dataGridView1.Columns.Count; j++)
            {
                table.AddCell(new Phrase(dataGridView1.Columns[j].HeaderText, FontFactory.GetFont("Arial", 10, iTextSharp.text.Font.BOLD)));
            }
            for (int i = 0; i < dataGridView1.Rows.Count; i++)
            {
                table.WidthPercentage = 100;
                for (int j = 0; j < dataGridView1.Columns.Count; j++)
                {
                    if (dataGridView1[j, i].Value != null)
                    {
                        table.AddCell(new Phrase(dataGridView1[j, i].Value.ToString(), FontFactory.GetFont("Arial", 10, BaseColor.BLACK)));
                    }
                }
            }
            dc.Add(table);
            dc.Close();
        }
    }
}
