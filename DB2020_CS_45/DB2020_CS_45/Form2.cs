﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;


namespace DB2020_CS_45
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "" && textBox4.Text != "" && dateTimePicker1.Text != "" && textBox6.Text != "" && comboBox1.Text != "")
            {
                if (Regex.IsMatch(textBox1.Text, @"(^[2]{1}[0]{1}[0-9]{2}-[A-Z]{2}-[0-9]{3}$)"))
                {
                    if (Regex.IsMatch(textBox4.Text, @"^(?:\\d{2}-\\d{3}-\\d{3}-\\d{3}|\\d{11})$"))
                    {
                        if (Regex.IsMatch(textBox6.Text, @"^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$"))
                        {
                            exceptioncheck1();
                        }
                        else
                        {
                            MessageBox.Show("Please correct the format of the email");
                        }
                    }
                    else
                    {
                        MessageBox.Show("Enter correct contact number");
                    }

                }
                else
                {
                    MessageBox.Show("Please correct the format of the registration number");
                }
            }
            else
            {
                MessageBox.Show("Please fill all the fields");
            }

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }




        private void exceptioncheck1()
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Insert into Person values (@FirstName,@LastName,@Contact,@Email,@DateOfBirth,@Gender)", con);
            cmd.Parameters.AddWithValue("@FirstName", textBox2.Text);
            cmd.Parameters.AddWithValue("@LastName", textBox3.Text);

            cmd.Parameters.AddWithValue("@Contact", textBox4.Text);

            cmd.Parameters.AddWithValue("@Email", textBox6.Text);
            cmd.Parameters.AddWithValue("@DateOfBirth", dateTimePicker1.Value);
            cmd.Parameters.AddWithValue("@Gender", int.Parse(comboBox1.Text));
            cmd.ExecuteNonQuery();
            SqlCommand far = new SqlCommand("Insert into Student values (@Id,@RegistrationNo)", con);


            SqlCommand farr = new SqlCommand("Select MAX(Id) from Person ", con);
            far.Parameters.AddWithValue("@Id", int.Parse(farr.ExecuteScalar().ToString()));
            far.Parameters.AddWithValue("@RegistrationNo", textBox1.Text);
            far.ExecuteNonQuery();

            MessageBox.Show("Successfully saved");
        }
    }
}
