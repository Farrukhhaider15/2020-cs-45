﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Text.RegularExpressions;

namespace DB2020_CS_45
{
    public partial class Form10 : Form
    {
        public Form10()
        {
            InitializeComponent();
            xyz();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }
        public void xyz()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Evaluation.Id from Evaluation except  (select  Evaluation.Id from  Evaluation  join GroupEvaluation on Evaluation.Id=GroupEvaluation.EvaluationId join [Group] on GroupEvaluation.GroupId=@GroupId)", con);
            cmd.Parameters.AddWithValue("@GroupId", comboBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);

            DataSet ds = new DataSet();
            da.Fill(ds, "Evaluation");
            comboBox2.ValueMember = "EvaluationId";
            comboBox2.DataSource = ds.Tables["Evaluation"];


            SqlCommand cmd1 = new SqlCommand("Select [Group].Id FROM [Group]", con);
            
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataSet ds1 = new DataSet();
            da1.Fill(ds1, "[Group]");
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = ds1.Tables["[Group]"];

        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" )
            {
                Regex nonNumericRegex = new Regex(@"[1-9]\d*");
                if (nonNumericRegex.IsMatch(textBox1.Text))
                {

                    var con = Configuration.getInstance().getConnection();
                    SqlCommand cmd1 = new SqlCommand("Select TotalMarks from Evaluation where Evaluation.Id=@EvaluationId", con);
                    cmd1.Parameters.AddWithValue("@EvaluationId", comboBox2.Text);
                    cmd1.ExecuteNonQuery();
                    string c = cmd1.ExecuteScalar().ToString();
                    if (int.Parse(c) >= int.Parse(textBox1.Text))
                    {
                        adddata();
                    }
                    else
                    {
                        MessageBox.Show("Enter correct marks");
                    }




                }

                else
                {
                    MessageBox.Show("Please enter numeric data");
                }
                

            }
            else
            {
                MessageBox.Show("Please enter all the  data");
            }

;
        }
        private void adddata()
        {
            var con = Configuration.getInstance().getConnection();
  
            SqlCommand cmd = new SqlCommand("Insert into GroupEvaluation values (@GroupId,@EvaluationId,@ObtainedMarks,@EvaluationDate)", con);
            cmd.Parameters.AddWithValue("@GroupId", comboBox1.Text);
            cmd.Parameters.AddWithValue("@EvaluationId", comboBox2.Text);
        
                cmd.Parameters.AddWithValue("@ObtainedMarks", textBox1.Text);
            
            cmd.Parameters.AddWithValue("@EvaluationDate", DateTime.Now);
            cmd.ExecuteNonQuery();
           

           






            MessageBox.Show("Successfully saved");
        }

        private void Form10_Load(object sender, EventArgs e)
        {

        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("select Evaluation.Id from Evaluation except  (select  Evaluation.Id from  Evaluation  join GroupEvaluation on Evaluation.Id=GroupEvaluation.EvaluationId join [Group] on GroupEvaluation.GroupId=@GroupID)", con);
            cmd.Parameters.AddWithValue("@GroupID", comboBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "Evaluation");
            comboBox2.ValueMember = "ID";
            comboBox2.DataSource = ds.Tables["Evaluation"];
        }
    }
}
