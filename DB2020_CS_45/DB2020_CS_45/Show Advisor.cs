﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_45
{
    public partial class Show_Advisor : Form
    {
        public Show_Advisor()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Designation,Id,Salary from Advisor ", con);
            cmd.Parameters.AddWithValue("@Id", textBox1.Text);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void Show_Advisor_Load(object sender, EventArgs e)
        {

        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Advisor SET Designation= @Designation,Id= @Id,Salary= @Salary WHERE Id=@Id", con);
            cmd.Parameters.AddWithValue("@Designation", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[0].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@Id", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
            cmd.Parameters.AddWithValue("@Salary", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString());
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data Updated Successfully");

        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

          
            SqlCommand cmd4 = new SqlCommand("DELETE ProjectAdvisor WHERE AdvisorId= @Id", con);
            //SqlCommand cmd = new SqlCommand("DELETE Student WHERE RegistrationNo= @RegistrationNo", con);
            
            
            SqlCommand cmd2 = new SqlCommand("DELETE Advisor WHERE Id= @Id", con);

            
            cmd4.Parameters.AddWithValue("@Id", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
            cmd2.Parameters.AddWithValue("@Id", textBox1.Text);
            







        
        
            cmd4.ExecuteNonQuery();

            cmd2.ExecuteNonQuery();
            MessageBox.Show("Data Deleted Successfully");
        }

    }
}
