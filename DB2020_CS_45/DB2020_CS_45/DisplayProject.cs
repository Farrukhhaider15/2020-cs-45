﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_45
{
    public partial class DisplayProject : Form
    {
        public DisplayProject()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id,Description,Title from Project ", con);
       
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("UPDATE Project SET Description= @Description,Title= @Title where Id=@Id ", con);
            cmd.Parameters.AddWithValue("@Id", textBox1.Text);
            cmd.Parameters.AddWithValue("@Description", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[1].FormattedValue.ToString());
         
            cmd.Parameters.AddWithValue("@Title", dataGridView1.Rows[dataGridView1.CurrentRow.Index].Cells[2].FormattedValue.ToString());
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data Updated Successfully");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("DELETE GroupProject WHERE ProjectId= @Id", con);
            SqlCommand cmd = new SqlCommand("DELETE Project WHERE Id= @Id", con);
           
            cmd1.Parameters.AddWithValue("@Id", textBox1.Text);
            cmd.Parameters.AddWithValue("@Id", textBox1.Text);
            cmd1.ExecuteNonQuery();
            cmd.ExecuteNonQuery();
            MessageBox.Show("Data Deleted Successfully");
        }
    }
}
    