﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_45
{
    public partial class InsertDataForm2 : Form
    {
        public InsertDataForm2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {

        }

        private void dateTimePicker1_ValueChanged(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Insert into Project values (@Description,@Title)", con);
            //cmd.Parameters.AddWithValue("@Id", textBox1.Text);
            cmd.Parameters.AddWithValue("@Description", textBox4.Text);
            cmd.Parameters.AddWithValue("@Title", textBox6.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }
    }
}
