﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_45
{
    public partial class Form7 : Form
    {
        public Form7()
        {
            InitializeComponent();
             xyz1();
        }

        private void xyz1()
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd1 = new SqlCommand("Select Id FROM Advisor", con);
            SqlDataAdapter da1 = new SqlDataAdapter(cmd1);
            DataSet ds1 = new DataSet();
            da1.Fill(ds1, "Advisor");
            comboBox2.ValueMember = "Id";
            comboBox2.DataSource = ds1.Tables["Advisor"];

         
            SqlCommand cmd2 = new SqlCommand("Select Id FROM Advisor", con);
            SqlDataAdapter da2 = new SqlDataAdapter(cmd2);
            DataSet ds2 = new DataSet();
            da2.Fill(ds2, "Advisor");
            comboBox3.ValueMember = "Id";
            comboBox3.DataSource = ds2.Tables["Advisor"];

            SqlCommand cmd3 = new SqlCommand("Select Id FROM Advisor", con);
            SqlDataAdapter da3 = new SqlDataAdapter(cmd3);
            DataSet ds3 = new DataSet();
            da3.Fill(ds3, "Advisor");
            comboBox4.ValueMember = "Id";
            comboBox4.DataSource = ds3.Tables["Advisor"];
          


            SqlCommand cmd = new SqlCommand("select Project.Id from  Project Except (SELECT ProjectAdvisor.ProjectId FROM  ProjectAdvisor)", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds, "Project");
            comboBox1.ValueMember = "Id";
            comboBox1.DataSource = ds.Tables["Project"];

            comboBox5.Items.Add("11");

            comboBox6.Items.Add("12");
            comboBox7.Items.Add("14");
        }
        private void button1_Click(object sender, EventArgs e)
        {
            if (comboBox2.Text!=comboBox3.Text && comboBox3.Text!=comboBox4.Text) 
            {

                var con = Configuration.getInstance().getConnection();

                SqlCommand cmd = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorId,@ProjectId,@AdvisorRole,@AssignmentDate)", con);
                //  SqlCommand farr = new SqlCommand("Select Id from Project ", con);
                // SqlCommand farrukh = new SqlCommand("Select Id from Advisor ", con);
                cmd.Parameters.AddWithValue("@AdvisorId", comboBox2.Text);
                cmd.Parameters.AddWithValue("@AdvisorRole", comboBox5.Text);
                cmd.Parameters.AddWithValue("@ProjectId", comboBox1.Text);
                cmd.Parameters.AddWithValue("@AssignmentDate", DateTime.Now);



                cmd.ExecuteNonQuery();

                SqlCommand cmd1 = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorId1,@ProjectId1,@AdvisorRole1,@AssignmentDate1)", con);
                //   SqlCommand farr1 = new SqlCommand("Select Id from Project ", con);
                //  SqlCommand farrukh1 = new SqlCommand("Select Id from Advisor ", con);
                cmd1.Parameters.AddWithValue("@AdvisorId1", comboBox3.Text);
                cmd1.Parameters.AddWithValue("@AdvisorRole1", comboBox6.Text);
                cmd1.Parameters.AddWithValue("@ProjectId1", comboBox1.Text);
                cmd1.Parameters.AddWithValue("@AssignmentDate1", DateTime.Now);
                cmd1.ExecuteNonQuery();

                SqlCommand cmd2 = new SqlCommand("Insert into ProjectAdvisor values (@AdvisorId2,@ProjectId2,@AdvisorRole2,@AssignmentDate2)", con);
                //  SqlCommand farr2 = new SqlCommand("Select Id from Project ", con);
                //   SqlCommand farrukh2 = new SqlCommand("Select Id from Advisor ", con);
                cmd2.Parameters.AddWithValue("@AdvisorId2", comboBox4.Text);
                cmd2.Parameters.AddWithValue("@AdvisorRole2", comboBox7.Text);
                cmd2.Parameters.AddWithValue("@ProjectId2", comboBox1.Text);
                cmd2.Parameters.AddWithValue("@AssignmentDate2", DateTime.Now);

                cmd2.ExecuteNonQuery();











                MessageBox.Show("Successfully saved");
            }

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
