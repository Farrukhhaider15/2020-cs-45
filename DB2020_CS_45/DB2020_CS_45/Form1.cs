﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace DB2020_CS_45
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            SubMenuHide();
        }

        private void SubMenuHide()
        {
            FirstSubMenu.Visible = false;
        }
        private void showsubmenu(Panel submenu)
        {

            if (submenu.Visible == false)
            {
                SubMenuHide();
                submenu.Visible = true;
            }
            else
            {
                submenu.Visible = false;
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void panel1_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            Form3 from1 = new Form3();
            from1.Show();
        }

        private void FirstSubMenu_Paint(object sender, PaintEventArgs e)
        {

        }

        private void ManageStudentButton_Click(object sender, EventArgs e)
        {
            showsubmenu(FirstSubMenu);
        }

        private void button3_Click_1(object sender, EventArgs e)
        {
            Form2 from = new Form2();
            from.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            Form3 from1 = new Form3();
            from1.Show();
        }

        private void button5_Click(object sender, EventArgs e)
        {
            Form3 from4= new Form3();
            from4.Show();

        }

        private void panel2_Paint(object sender, PaintEventArgs e)
        {

        }

        private void button8_Click(object sender, EventArgs e)
        {
            InsertDataForm1 from = new InsertDataForm1();
            from.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        {
            Show_Advisor shows = new Show_Advisor();
            shows.Show();
        }

        private void button10_Click(object sender, EventArgs e)
        {
            Show_Advisor shows = new Show_Advisor();
            shows.Show();
        }

        private void button11_Click(object sender, EventArgs e)
        {
            Show_Advisor shows = new Show_Advisor();
            shows.Show();
        }

        private void button12_Click(object sender, EventArgs e)
        {
            InsertDataForm2 from2 = new InsertDataForm2();
            from2.Show();
        }

        private void button13_Click(object sender, EventArgs e)
        {

            DisplayProject from2 = new DisplayProject();
            from2.Show();
        }

        private void button15_Click(object sender, EventArgs e)
        {
            DisplayProject from2 = new DisplayProject();
            from2.Show();
        }

        private void button14_Click(object sender, EventArgs e)
        {
            DisplayProject from2 = new DisplayProject();
            from2.Show();
        }

        private void button16_Click(object sender, EventArgs e)
        {
            Form5 groups = new Form5();
            groups.Show();
        }

        private void button17_Click(object sender, EventArgs e)
        {
            Form6 groupProject = new Form6();
            groupProject.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button18_Click(object sender, EventArgs e)
        {
            Form7 Projectadvisor = new Form7();
            Projectadvisor.Show();
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Form8 evaluation = new Form8();
            evaluation.Show();
        }

        private void button20_Click(object sender, EventArgs e)
        {
            Form10 groupEvaluation = new Form10();
            groupEvaluation.Show();
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
